import * as React from 'react';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import AppNavigation from './App/AppNavigation';
import configureAppStore from './App/redux/store';
import firestore from '@react-native-firebase/firestore';
import {SafeAreaView} from 'react-native';

export const db = firestore();
const store = configureAppStore();
export const isOrderer = false;

function App() {
  return (
    <Provider store={store}>
      <NavigationContainer
        theme={{
          dark: false,
          colors: {
            background: 'white',
            card: 'white',
          },
        }}>
        <SafeAreaView style={{flex: 1}}>
          <AppNavigation/>
        </SafeAreaView>
      </NavigationContainer>
    </Provider>
  );
}

export default App;
