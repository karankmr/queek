import * as React from 'react';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import BottomTabScreen from './Containers/BottomTabScreen';
import LoginScreen from './Containers/LoginScreen';
import useAuth from './hooks/useAuth';

// Initialize
const Stack = createNativeStackNavigator();

function AppNavigation() {
  const {initializing, user} = useAuth();

  if (initializing) {
    return null;
  }

  return (
    <Stack.Navigator
      initialRouteName={`${user ? 'BottomTabScreen' : 'LoginScreen'}`}>
      <Stack.Screen
        name="BottomTabScreen"
        options={{headerShown: false}}
        component={BottomTabScreen}
      />
      <Stack.Screen name="LoginScreen" component={LoginScreen} />
    </Stack.Navigator>
  );
}

export default AppNavigation;
