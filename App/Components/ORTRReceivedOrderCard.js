import {Image, StyleSheet, Text, View} from "react-native";
import * as React from "react";
import Color from "../Theme/Color";
import Fonts from "../Theme/Fonts";
import FontSizes from "../Theme/FontSizes";
import Button from "./Button";
import {ORDER_STATUS} from "../constants";
import {useState} from "react";
import Collapsible from "react-native-collapsible";

const HEADER_HEIGHT = 44

function ORTRReceivedOrderCard({onPress, containerStyle, status}) {
  const [isCollapsed, setIsCollapsed] = useState(true)
  const [decisionTaken, setDecisionTaken] = useState(false)

  const requesterItem = (el) => {
    return (
      <View key={el}>
        <View style={styles.separator}/>
        <View style={styles.collapsedItem}>
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
            <Image source={{uri: 'https://picsum.photos/200'}} style={styles.profileImage}/>
            <View style={{justifyContent: 'space-between', height: HEADER_HEIGHT}}>
              <Text style={styles.requesterName}>Nick Gibson</Text>
              <Text style={styles.requestedTime}>requested 20 minutes ago</Text>
            </View>
          </View>

          <View style={{flexDirection: 'row'}}>
            <Button onPress={() => {
              setDecisionTaken(true)
              setIsCollapsed(true)
            }} text={'Accept'}
                    containerStyle={{...styles.acceptRejectButtons, marginRight: 6, backgroundColor: Color.GREEN}}/>
            <Button onPress={() => {
              setDecisionTaken(true)
              setIsCollapsed(true)
            }} text={'Reject'}
                    containerStyle={{...styles.acceptRejectButtons, marginLeft: 6, backgroundColor: Color.RED}}/>
          </View>
        </View>
      </View>
    )
  }

  return (
    <View onPress={onPress} style={{...styles.container, ...containerStyle}}>
      <View style={styles.detailsContainer}>
        <View>
          <Image source={{uri: 'https://picsum.photos/200'}} style={styles.itemImage}/>
        </View>
        <View style={{flex: 1, justifyContent: 'space-between'}}>
          <Text numberOfLines={2} ellipsizeMode='tail' style={styles.productName}>Lowrance HDC Carbon 12 Fish
            Finder/ChartPlotter
            Combo </Text>
          <View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.travelDetailsTitle}>Deliver to </Text>
              <Text style={styles.travelDetails}>Delhi</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.travelDetailsTitle}>Deliver from </Text>
              <Text style={styles.travelDetails}>London</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.travelDetailsTitle}>Before </Text>
              <Text style={styles.travelDetails}>Feb 22, 2020</Text>
            </View>
          </View>
        </View>
      </View>

      <View style={styles.travelerReward}>
        <Text style={styles.travelerRewardText}>Traveller reward</Text>
        <Text style={styles.reward}>$120</Text>
      </View>

      {decisionTaken ? <View style={styles.statusContainer}>
          <Text style={styles.statusName}>Status</Text>
          <Text style={[styles.statusName, {color: ORDER_STATUS[status].color}]}>{ORDER_STATUS[status].text}</Text>
        </View> :
        <Button
          iconName={isCollapsed ? 'chevron-down' : 'chevron-up'}
          iconColor={'white'}
          iconSize={36}
          onPress={() => setIsCollapsed(!isCollapsed)}
          text={isCollapsed ? 'View travellers' : 'Hide travellers'}
          containerStyle={{marginTop: 16}}
        />
      }
      <Collapsible collapsed={isCollapsed}>
        {[1, 2].map(el => requesterItem(el))}
      </Collapsible>
    </View>
  );
}

const styles = StyleSheet.create({
  separator: {
    width: '100%',
    height: 1,
    backgroundColor: Color.LightGrey2,
    marginTop: 16,
  },
  collapsedItem: {
    flexDirection: 'column',
    marginTop: 16,
  },
  requesterName: {
    fontSize: FontSizes.medium - 6,
    color: 'black',
    fontFamily: Fonts.Foundation,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  container: {
    width: '100%',
    padding: 16,
    borderRadius: 6,
    borderColor: Color.LightGrey3,
    borderWidth: 1,
    marginTop: 16
  },
  acceptRejectButtons: {
    marginTop: 16,
    width: 1,
    flexGrow: 1
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  detailsContainer: {
    flexDirection: 'row',
    flex: 1
  },
  productName: {
    fontSize: FontSizes.small - 2,
    color: 'black',
    fontFamily: Fonts.Foundation,
  },
  travelDetailsTitle: {
    fontSize: FontSizes.small - 4,
    color: Color.LightGrey1,
    fontFamily: Fonts.Feather,
  },
  travelDetails: {
    fontSize: FontSizes.small - 4,
    color: 'black',
    fontFamily: Fonts.Feather
  },
  profileImage: {
    width: HEADER_HEIGHT,
    height: HEADER_HEIGHT,
    borderRadius: 200,
    marginRight: 16
  },
  itemImage: {
    width: 80,
    height: 80,
    borderRadius: 6,
    marginRight: 16,
    borderColor: Color.LightGrey3,
    borderWidth: 1
  },
  travellerName: {
    fontSize: FontSizes.medium - 6,
    color: 'black',
    fontFamily: Fonts.Foundation,
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  reward: {
    fontSize: FontSizes.medium - 6,
    color: 'black',
    fontFamily: Fonts.Foundation,
    justifyContent: 'space-between'
  },
  requestedTime: {
    fontSize: FontSizes.small - 1,
    color: Color.LightGrey1,
    fontFamily: Fonts.FontAwesome,
  },
  travelerRewardText: {
    fontSize: FontSizes.small,
    color: Color.LightGrey1,
    fontFamily: Fonts.FontAwesome,
  },
  travelerReward: {
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  statusContainer: {
    marginTop: 12,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  statusName: {
    fontSize: FontSizes.small,
    color: 'black',
    fontFamily: Fonts.Foundation,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 4
  },
});
export default ORTRReceivedOrderCard