import React, {useState} from 'react';
import {Alert, Platform, StyleSheet, View} from 'react-native';
import PseudoInput from './PseudoInput';
import Color from '../Theme/Color';
import moment from 'moment';
import Button from './Button';
import DateTimePicker from '@react-native-community/datetimepicker';
import FontSizes from '../Theme/FontSizes';
import Fonts from '../Theme/Fonts';
import AutoSearchInput from './AutoSearchInput';
import mapMyIndia from 'mapmyindia-restapi-react-native-beta/index';
import {isValidFormValues} from '../utils';
import {useSelector} from 'react-redux';
import {getUserDetails} from '../redux/selectors/user';
import {addTravel} from '../handlers/travel';

const initialOrderFormData = {
  from: '',
  destination: '',
  date: new Date(),
};

const TRAddTravelDetailsForm = () => {
  const [formData, setFormData] = useState(initialOrderFormData);
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const user = useSelector(getUserDetails);
  const [autoSearchData, setAutoSearchData] = useState({
    name: '',
    data: [],
  });
  const [dateChosenStatus, setDateChosenStatus] = useState({
    date: false,
    time: false,
  });

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || formData.date;
    setFormData(prev => ({...prev, date: currentDate}));
    setShow(Platform.OS === 'ios');
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
    setDateChosenStatus({...dateChosenStatus, date: true});
  };

  const showTimepicker = () => {
    showMode('time');
    setDateChosenStatus({...dateChosenStatus, time: true});
  };

  const autoSearch = (key, value) => {
    console.log('VALUE', value);
    if (!value || !value.length) {
      setAutoSearchData({
        name: '',
        data: [],
      });
      console.log('INSIDE EMPTY');
    } else {
      mapMyIndia.atlas_auto({query: value}, response => {
        console.log(response);
        setAutoSearchData({
          name: key,
          data: response?.suggestedLocations || [],
        });
      });
      console.log('INSIDE NOT EMPTY');
    }
  };

  const inputChangeHandler = (key, value) => {
    setFormData(prev => ({...prev, [key]: value}));
    if (key === 'from' || key === 'destination') {
      autoSearch(key, value);
    }
  };

  const getData = key => {
    if (autoSearchData.name === key) {
      return autoSearchData.data;
    } else {
      return [];
    }
  };

  const onItemClick = (key, value) => {
    setFormData(prev => ({
      ...prev,
      [key]: value,
    }));
    setAutoSearchData({
      name: '',
      data: [],
    });
  };

  const onSubmit = () => {
    console.log('FORM', formData);
    const {isError, errorMessage} = isValidFormValues(formData);
    if (isError) {
      return Alert.alert(errorMessage);
    }
    addTravel(formData, user).then(() => {
      setFormData(initialOrderFormData);
    });
  };

  return (
    <>
      <AutoSearchInput
        name={'from'}
        value={formData.from}
        onChange={v => inputChangeHandler('from', v)}
        placeholder={'From'}
        data={getData('from')}
        onItemClick={onItemClick}
      />

      <AutoSearchInput
        name={'destination'}
        value={formData.destination}
        onChange={v => inputChangeHandler('destination', v)}
        placeholder={'To'}
        data={getData('destination')}
        onItemClick={onItemClick}
      />
      <View style={styles.dateAndTimeContainer}>
        <PseudoInput
          onPress={showDatepicker}
          textColor={dateChosenStatus.date ? 'black' : Color.LightGrey1}
          placeholder={
            !dateChosenStatus.date
              ? 'Date'
              : moment(formData.date).format('DD-MMM-YYYY')
          }
          containerStyle={{flex: 1, marginRight: 6}}
        />
        <PseudoInput
          onPress={showTimepicker}
          textColor={dateChosenStatus.time ? 'black' : Color.LightGrey1}
          placeholder={
            !dateChosenStatus.time
              ? 'Time'
              : moment(formData.date).format('ddd, LT')
          }
          containerStyle={{flex: 1, marginLeft: 6}}
        />
      </View>
      {show && (
        <DateTimePicker
          testID="dateTimePicker2"
          value={formData.date}
          mode={mode}
          // is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}
      <Button onPress={onSubmit} text={'Add trip'}/>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  tagLine: {
    fontSize: FontSizes.large - 4,
    color: 'black',
    fontFamily: Fonts.EloquiaDisplayExtraBold,
    lineHeight: 40,
  },
  dateAndTimeContainer: {
    flexDirection: 'row',
  },
});

export default TRAddTravelDetailsForm;
