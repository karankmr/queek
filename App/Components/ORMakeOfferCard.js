import {Image, StyleSheet, Text, View} from 'react-native';
import * as React from 'react';
import Color from '../Theme/Color';
import Fonts from '../Theme/Fonts';
import FontSizes from '../Theme/FontSizes';
import Button from './Button';
import {getDateOrTimeFromNow, getStringDate} from '../utils';

const HEADER_HEIGHT = 44;

function ORMakeOfferCard({onPress, containerStyle, order}) {
  return (
    <View onPress={onPress} style={{...styles.container, ...containerStyle}}>
      <View style={styles.header}>
        <Image
          source={{uri: 'https://picsum.photos/200'}}
          style={styles.profileImage}
        />
        <View style={{justifyContent: 'space-between', height: HEADER_HEIGHT}}>
          <Text style={styles.travellerName}>Nick Gibson</Text>
          <Text style={styles.requestedTime}>
            requested {getDateOrTimeFromNow(order.postedDate)}
          </Text>
        </View>
      </View>

      <View style={styles.detailsContainer}>
        {/*<View>*/}
        {/*  <Image source={{uri: 'https://picsum.photos/200'}} style={styles.itemImage}/>*/}
        {/*</View>*/}
        <View style={{flex: 1, justifyContent: 'space-between'}}>
          {/*<Text numberOfLines={2} ellipsizeMode='tail' style={styles.productName}>Lowrance HDC Carbon 12 Fish*/}
          {/*  Finder/ChartPlotter*/}
          {/*  Combo </Text>*/}
          <View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.travelDetailsTitle}>Deliver to - </Text>
              <Text style={styles.travelDetails}>{order.from}</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.travelDetailsTitle}>Deliver from - </Text>
              <Text style={styles.travelDetails}>{order.destination}</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Text style={styles.travelDetailsTitle}>Before - </Text>
              <Text style={styles.travelDetails}>
                {getStringDate(order.beforeDate)}
              </Text>
            </View>
          </View>
        </View>
      </View>

      <View style={styles.travelerReward}>
        <Text style={styles.travelerRewardText}>Traveller reward</Text>
        <Text style={styles.reward}>${order.rewardPrice}</Text>
      </View>

      <Button
        onPress={null}
        text={'Make offer'}
        containerStyle={{marginTop: 16}}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    padding: 16,
    borderRadius: 6,
    borderColor: Color.LightGrey3,
    borderWidth: 1,
    marginTop: 16,
  },
  header: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  detailsContainer: {
    marginTop: 24,
    flexDirection: 'row',
    flex: 1,
  },
  productName: {
    fontSize: FontSizes.small - 2,
    color: 'black',
    fontFamily: Fonts.Foundation,
  },
  travelDetailsTitle: {
    fontSize: FontSizes.small,
    color: Color.LightGrey1,
    fontFamily: Fonts.Feather,
  },
  travelDetails: {
    fontSize: FontSizes.small,
    color: 'black',
    fontFamily: Fonts.Feather,
    textTransform: 'capitalize',
  },
  profileImage: {
    width: HEADER_HEIGHT,
    height: HEADER_HEIGHT,
    borderRadius: 200,
    marginRight: 16,
  },
  itemImage: {
    width: 80,
    height: 80,
    borderRadius: 6,
    marginRight: 16,
    borderColor: Color.LightGrey3,
    borderWidth: 1,
  },
  travellerName: {
    fontSize: FontSizes.medium - 6,
    color: 'black',
    fontFamily: Fonts.Foundation,
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  reward: {
    fontSize: FontSizes.medium - 6,
    color: 'black',
    fontFamily: Fonts.Foundation,
    justifyContent: 'space-between',
  },
  requestedTime: {
    fontSize: FontSizes.small - 1,
    color: Color.LightGrey1,
    fontFamily: Fonts.FontAwesome,
  },
  travelerRewardText: {
    fontSize: FontSizes.small,
    color: Color.LightGrey1,
    fontFamily: Fonts.FontAwesome,
  },
  travelerReward: {
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default ORMakeOfferCard;
