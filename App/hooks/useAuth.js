import React, {useEffect, useState} from 'react';
import auth from '@react-native-firebase/auth';
import {useDispatch, useSelector} from 'react-redux';
import {setUserDetails} from '../redux/actions/user';
import {userSchema} from '../constants/schema';
import {getUserDetails} from '../redux/selectors/user';
import _ from 'lodash';

const useAuth = () => {
  const [initializing, setInitializing] = useState(true);
  const [user, setUser] = useState(null);
  const storedUser = useSelector(getUserDetails);
  const dispatch = useDispatch();

  // Handle user state changes
  function onAuthStateChanged(user) {
    if (user && user._user) {
      console.log('storedUser', storedUser, user._user);
      setUser(user._user);
      if (user._user && !Object.keys(storedUser).length) {
        dispatch(setUserDetails(userSchema(user._user)));
      }
    }
    if (initializing) setInitializing(false);
  }

  useEffect(() => {
    const subscriber = auth().onAuthStateChanged(onAuthStateChanged);
    return subscriber; // unsubscribe on unmount
  }, []);

  return {initializing, user};
};

export default useAuth;
