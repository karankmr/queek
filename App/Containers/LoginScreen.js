import React, {useEffect, useState} from 'react';
import {View, StyleSheet, TextInput, Alert} from 'react-native';
import {AsYouType, isValidPhoneNumber} from 'libphonenumber-js';
import auth from '@react-native-firebase/auth';
import Button from '../Components/Button';
import useAuth from '../hooks/useAuth';
import {createNewUser} from '../handlers/user';

const nonNumeric = /[^0-9]/gi;
const COUNTRY_CODE = '+91';

const formatPhoneNumber = s =>
  new AsYouType('US').input(s.replace(nonNumeric, ''));

const getRealNumber = s => {
  const asYouType = new AsYouType();
  asYouType.input(s);
  return asYouType.getChars();
};

const LoginScreen = ({navigation}) => {
  const [confirm, setConfirm] = useState(null);
  const [phone, setPhone] = useState('');
  const [code, setCode] = useState('');
  const {user} = useAuth();

  const handleChange = e => {
    setPhone(formatPhoneNumber(e));
  };
  // Phone Authentication process
  const onSubmit = async () => {
    if (isValidPhoneNumber(phone, 'IN')) {
      const realPhone = getRealNumber(phone);
      const confirmation = await auth().signInWithPhoneNumber(
        `${COUNTRY_CODE}${realPhone}`,
      );
      setConfirm(confirmation);
    } else {
      Alert.alert('Enter a valid phone number.');
    }
  };

  // Confirm user enter code
  const confirmCode = async () => {
    try {
      await confirm.confirm(code);
    } catch (error) {
      Alert.alert('Invalid code.');
      console.log('Invalid code.');
    }
  };

  useEffect(() => {
    if (user) {
      createNewUser(user).then(() => {
        navigation.replace('BottomTabScreen');
      });
    }
  }, [user]);

  if (!confirm) {
    return (
      <View style={styles.container}>
        <TextInput
          onChangeText={handleChange}
          style={styles.phoneNumberInput}
          keyboardType="numeric"
          placeholder="Phone Number"
          defaultValue={phone}
        />
        <Button
          containerStyle={styles.containerStyle}
          text={'Send Code'}
          onPress={onSubmit}
        />
      </View>
    );
  }

  return (
    <View style={styles.container}>
      <TextInput
        value={code}
        style={styles.phoneNumberInput}
        onChangeText={setCode}
        keyboardType="numeric"
        placeholder="Enter Code"
      />
      <Button
        containerStyle={styles.containerStyle}
        text="Confirm Code"
        onPress={confirmCode}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: '10%',
  },
  phoneNumberInput: {
    borderRadius: 72,
    fontSize: 14,
    padding: 20,
    borderStyle: 'solid',
    borderWidth: 1,
    width: '100%',
  },
  containerStyle: {
    width: '50%',
  },
});

export default LoginScreen;
