import {ScrollView, StyleSheet, View} from "react-native";
import * as React from "react";
import ORTRReceivedOrderCard from "../Components/ORTRReceivedOrderCard";

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  }
});

function OROrdererReceivedFromTravellerScreen() {
  return (
    <ScrollView style={styles.container}>
      <ORTRReceivedOrderCard status={'ACCEPTED'}/>
      <View style={{height: 100}}/>
    </ScrollView>
  );
}

export default OROrdererReceivedFromTravellerScreen
