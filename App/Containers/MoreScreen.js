import {Text, TouchableOpacity, View} from 'react-native';
import * as React from 'react';
import auth from '@react-native-firebase/auth';
import {clearUserDetails} from '../redux/actions/user';
import {useDispatch} from 'react-redux';

function MoreScreen({navigation}) {
  const dispatch = useDispatch();

  const signOut = () => {
    auth()
      .signOut()
      .then(() => {
        dispatch(clearUserDetails({}));
        navigation.replace('LoginScreen');
      });
  };

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <Text style={{fontSize: 20, color: 'black'}}>MORE SCREEN</Text>
      <TouchableOpacity onPress={signOut}>
        <Text style={{fontSize: 20, textAlign: 'center'}}>Sign Out</Text>
      </TouchableOpacity>
    </View>
  );
}

export default MoreScreen;
