import {ScrollView, StyleSheet, Text, View} from 'react-native';
import * as React from 'react';
import FontSizes from '../Theme/FontSizes';
import Fonts from '../Theme/Fonts';
import Button from '../Components/Button';
import TRAddTravelDetailsForm from '../Components/TRAddTravelDetailsForm';

function TRCreateTripScreen() {
  return (
    <ScrollView style={styles.container}>
      <Text style={styles.tagLine}>
        Add your trip details to start earning money
      </Text>
      <Button
        onPress={null}
        text={'One way'}
        containerStyle={{width: '50%', marginBottom: 20}}
      />
      <TRAddTravelDetailsForm/>
      <View style={{height: 100}}/>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
  tagLine: {
    fontSize: FontSizes.large - 4,
    color: 'black',
    fontFamily: Fonts.EloquiaDisplayExtraBold,
    lineHeight: 40,
  },
  dateAndTimeContainer: {
    flexDirection: 'row',
  },
});

export default TRCreateTripScreen;
