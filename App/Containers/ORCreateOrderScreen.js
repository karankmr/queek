import * as React from 'react';
import {useEffect, useState} from 'react';
import {SafeAreaView, ScrollView, StyleSheet, Text, View} from 'react-native';
import ORMakeOfferCard from '../Components/ORMakeOfferCard';
import ORCreateOrderForm from '../Components/ORCreateOrderForm';
import FontSizes from '../Theme/FontSizes';
import Fonts from '../Theme/Fonts';
import {getOrders} from '../handlers/order';
import Button from "../Components/Button";
import {NativeModules} from 'react-native';

const {DigioModule} = NativeModules;

function ORCreateOrderScreen() {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    getOrders().then(data => {
      setOrders(data);
    });
  }, []);

  return (
    <ScrollView style={styles.container}>
      <Text style={styles.tagLine}>
        Shop Products from Abroad delivered to You by Travelers
      </Text>
      <Button
        onPress={() => {
          DigioModule.initiateDigioKycProcess()
        }}
        text={'Try api'}
        containerStyle={{marginTop: 16}}
      />
      <ORCreateOrderForm/>
      {orders.length
        ? orders.map(order => <ORMakeOfferCard key={order.id} order={order}/>)
        : null}
      <View style={{height: 100}}/>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  tagLine: {
    fontSize: FontSizes.large,
    color: 'black',
    fontFamily: Fonts.EloquiaDisplayExtraBold,
    lineHeight: 48,
  },
});

export default ORCreateOrderScreen;
