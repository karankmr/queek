import {ScrollView, StyleSheet, View} from "react-native";
import * as React from "react";
import ORTRRequestedOrderCard from "../Components/ORTRRequestedOrderCard";

function OROrdererRequestedToTravellerScreen() {
  return (
    <ScrollView style={styles.container}>
      <ORTRRequestedOrderCard status={'ACCEPTED'}/>
      <ORTRRequestedOrderCard status={'PENDING'}/>
      <ORTRRequestedOrderCard status={'REJECTED'}/>
      <View style={{height: 100}}/>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  }
});

export default OROrdererRequestedToTravellerScreen
