import {ScrollView, StyleSheet, View} from 'react-native';
import * as React from 'react';
import ORCreatedOrderCard from '../Components/ORCreatedOrderCard';
import {useEffect, useState} from 'react';
import {useSelector} from 'react-redux';
import {getUserDetails} from '../redux/selectors/user';
import {getOrdersById} from '../handlers/order';
import TripCard from "../Components/TripCard";

function TRCreatedTripScreen() {
  const [orders, setOrders] = useState([]);
  const user = useSelector(getUserDetails);

  useEffect(() => {
    if (user) {
      getOrdersById(user._id).then(data => {
        setOrders(data);
      });
    }
  }, []);

  console.log('USER', user, orders);

  return (
    <ScrollView style={styles.container}>
      <TripCard/>
      {orders.length
        ? orders.map(order => <ORCreatedOrderCard key={order.id} order={order}/>)
        : null}
      <View style={{height: 100}}/>
    </ScrollView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
  },
});

export default TRCreatedTripScreen;
