import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import * as React from 'react';
import ORAllRecentOrderScreen from './ORAllRecentOrderScreen';
import TRAllRecentTripsScreen from './TRAllRecentTripsScreen';
import MoreScreen from './MoreScreen';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import Color from '../Theme/Color';
import ORCreateOrderScreen from "./ORCreateOrderScreen";
import {isOrderer} from "../../App";
import TRCreateTripScreen from "./TRCreateTripScreen";

const Tab = createBottomTabNavigator();

function BottomTabScreen() {
  const bottomTabIconSize = 30;
  const bottomTabFontSize = 14;
  const bottomTabActiveColor = Color.ThemePurple;
  const bottomTabInActiveColor = Color.LightGrey1;
  return (
    <Tab.Navigator
      initialRouteName="Home"
      screenOptions={{
        tabBarStyle: {height: 66, paddingBottom: 10},
      }}>
      <Tab.Screen
        options={{
          headerShown: false,
          tabBarIcon: e => {
            return (
              <MaterialCommunityIcons
                name={'home'}
                size={bottomTabIconSize}
                color={
                  e.focused ? bottomTabActiveColor : bottomTabInActiveColor
                }
              />
            );
          },
          tabBarLabelStyle: {fontSize: bottomTabFontSize},
          tabBarActiveTintColor: Color.ThemePurple,
        }}
        name="Home"
        component={isOrderer ? ORCreateOrderScreen : TRCreateTripScreen}
      />
      {isOrderer ? <Tab.Screen
          name="Orders"
          component={ORAllRecentOrderScreen}
          options={{
            tabBarIcon: e => {
              return (
                <MaterialCommunityIcons
                  name={'bag-checked'}
                  size={bottomTabIconSize}
                  color={
                    e.focused ? bottomTabActiveColor : bottomTabInActiveColor
                  }
                />
              );
            },
            tabBarLabelStyle: {fontSize: bottomTabFontSize},
            tabBarActiveTintColor: Color.ThemePurple,
          }}
        /> :
        <Tab.Screen
          name="Trips"
          component={TRAllRecentTripsScreen}
          options={{
            tabBarIcon: e => {
              return (
                <MaterialCommunityIcons
                  name={'airplane'}
                  size={bottomTabIconSize}
                  color={
                    e.focused ? bottomTabActiveColor : bottomTabInActiveColor
                  }
                />
              );
            },
            tabBarLabelStyle: {fontSize: bottomTabFontSize},
            tabBarActiveTintColor: Color.ThemePurple,
          }}
        />}
      <Tab.Screen
        name="More"
        component={MoreScreen}
        options={{
          tabBarIcon: e => {
            return (
              <MaterialCommunityIcons
                name={'account'}
                size={bottomTabIconSize}
                color={
                  e.focused ? bottomTabActiveColor : bottomTabInActiveColor
                }
              />
            );
          },
          tabBarLabelStyle: {fontSize: bottomTabFontSize},
          tabBarActiveTintColor: Color.ThemePurple,
        }}
      />
    </Tab.Navigator>
  );
}

export default BottomTabScreen;
