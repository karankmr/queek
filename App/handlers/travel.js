import {travellerSchema} from '../constants/schema';
import {Alert} from 'react-native';
import {addTravelApi} from '../api/travel';
import {updateUserApi} from '../api/users';

export const addTravel = async (formData, user) => {
  try {
    const params = travellerSchema({
      ...formData,
      postedBy: user._id,
      travelDate: formData.date,
      postedDate: new Date(),
    });
    console.log('Params', params);
    const res = await addTravelApi(params);
    await updateUserApi(user._id, {
      trips: {[res.id]: {_id: res.id, requestedOffers: {}}},
    });
    Alert.alert('Trip Created');
  } catch (error) {
    console.error(error);
    Alert.alert(error.message);
  }
};
