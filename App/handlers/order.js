import {Alert} from 'react-native';
import {addOrderApi, getOrderApi, getOrdersByIdApi} from '../api/order';
import {orderSchema} from '../constants/schema';
import moment from 'moment';

export const addOrder = async (formData, user) => {
  try {
    const params = orderSchema({
      ...formData,
      postedBy: user._id,
      beforeDate: formData.date,
      postedDate: new Date(),
      productName: formData.title,
      productImageUrl: formData.imageUrl,
    });
    console.log('Params', params);
    const data = await addOrderApi(params);
    Alert.alert('Order Created');
  } catch (error) {
    console.error(error);
    Alert.alert(error.message);
  }
};

export const getOrders = async () => {
  try {
    const data = await getOrderApi();
    console.log('Data', data);
    return data;
  } catch (error) {
    console.error(error);
    Alert.alert(error.message);
  }
};

export const getOrdersById = async id => {
  try {
    const data = await getOrdersByIdApi(id);
    console.log('Data', data);
    return data;
  } catch (error) {
    console.error(error);
    Alert.alert(error.message);
  }
};
