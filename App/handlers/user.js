import {Alert} from 'react-native';
import {addNewUser, getUserByPhone} from '../api/users';
import {userSchema} from '../constants/schema';

export const createNewUser = async user => {
  try {
    const data = await getUserByPhone(user.phoneNumber);
    console.log('DATA', data);
    if (data.length) {
      return;
    }
    await addNewUser(userSchema(user));
  } catch (error) {
    console.error(error);
    Alert.alert(error.message);
  }
};
