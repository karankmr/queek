/*
 default database schema for user, post and review
*/

export const userSchema = initializeData => ({
  _id: initializeData.phoneNumber,
  name: '',
  aadhaarImageUrl: '',
  imageUrl: '',
  kycStatus: '',
  phone: initializeData.phoneNumber,
  requestedOrdersByMe: {},
  receivedOrdersByOthers: {},
  trips: {},
});

export const travellerSchema = initializeData => ({
  from: initializeData.from,
  destination: initializeData.destination,
  postedBy: initializeData.postedBy,
  travelDate: initializeData.travelDate,
  postedDate: initializeData.postedDate,
});

export const orderSchema = initializeData => ({
  from: initializeData.from,
  productName: initializeData.productName,
  productImageUrl: initializeData.productImageUrl,
  destination: initializeData.destination,
  modeOfTravel: initializeData.modeOfTravel,
  postedBy: initializeData.postedBy,
  beforeDate: initializeData.beforeDate,
  rewardPrice: initializeData.rewardPrice,
  postedDate: initializeData.postedDate,
  isPickedByTraveller: false,
});

export const reviewSchema = initializeData => ({
  _id: initializeData.id,
  description: initializeData.description,
  rating: initializeData.rating,
  submittedFor: initializeData.submittedFor,
  submittedBy: initializeData.submittedBy,
});
