import Color from '../Theme/Color';

export const constants = {
  app: 'APP',
};

/*
 @pending: documents not submitted yet
 @progress: documents submitted but waiting for confirmation from 3p aadhaar service
 @complete: documents submitted and verified by the 3P aadhaar service
 */
export const KYC_STATUS = {
  PENDING: 'pending',
  PROGRESS: 'progress',
  COMPLETE: 'complete',
};

export const OrderStatus = {
  PENDING: 'pending',
  ACCEPTED: 'accepted',
  REJECTED: 'rejected',
};

export const ORDER_STATUS = {
  PENDING: {text: 'Pending', color: Color.LightGrey1},
  ACCEPTED: {text: 'Accepted', color: Color.GREEN},
  REJECTED: {text: 'Rejected', color: Color.RED},
};
