const Color = {
  ThemePurple: '#4e9ebf',
  InActiveTab: 'rgb(164,209,241)',
  LightGrey1: '#949494',
  LightGrey2: '#c4c4c4',
  LightGrey3: '#d7d7d7',
  GREEN: '#9dc89b',
  RED: '#f56260'
}

export default Color