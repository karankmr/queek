import {createReducer} from '@reduxjs/toolkit';
import {setUserDetails, clearUserDetails} from '../actions/user';

const userReducer = createReducer({}, builder => {
  builder.addCase(setUserDetails, (state, action) => action.payload);
  builder.addCase(clearUserDetails, (state, action) => action.payload);
});

export default userReducer;
