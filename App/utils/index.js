import moment from 'moment';

const isValidDateAndTimer = d => {
  if (Object.prototype.toString.call(d) === '[object Date]') {
    // it is a date
    if (isNaN(d)) {
      return false;
    } else {
      return true;
    }
  } else {
    return false;
  }
};

export const getStringDate = date => moment(date).format('ll');

export const getDateOrTimeFromNow = date =>
  moment(date).startOf('hour').fromNow();

export const isValidFormValues = formData => {
  let errorMessage = '';
  let isError = false;
  for (let key of Object.keys(formData)) {
    if (key === 'imageUrl' && formData[key].length === 0) {
      isError = true;
      errorMessage = `Please upload image`;
      break;
    } else if (key === 'date' && !isValidDateAndTimer(formData[key])) {
      isError = true;
      errorMessage = 'Invalid date and time';
      break;
    } else if (formData[key].length === 0) {
      isError = true;
      errorMessage = `Invalid ${key} field`;
      break;
    }
  }
  return {isError, errorMessage};
};
