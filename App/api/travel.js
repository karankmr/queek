import {db} from '../../App';

const COLLECTION_NAME = 'traveller';

export const addTravelApi = params =>
  db.collection(COLLECTION_NAME).add(params);
