import {db} from '../../App';

const COLLECTION_NAME = 'orders';

export const addOrderApi = params =>
  db.collection(COLLECTION_NAME).doc().set(params);

export const getOrderApi = () =>
  db
    .collection('orders')
    .get()
    .then(snapshot =>
      snapshot.docs.map(doc => ({
        ...doc.data(),
        id: doc.id,
      })),
    );

export const getOrdersByIdApi = id =>
  db
    .collection('orders')
    .where('postedBy', '==', id)
    .get()
    .then(snapshot =>
      snapshot.docs.map(doc => ({
        ...doc.data(),
        id: doc.id,
      })),
    );
