import {db} from '../../App';

export const addNewUser = params =>
  db.collection('users').doc(params.phone).set(params);

export const getUserByPhone = phone =>
  db
    .collection('users')
    .where('loginId', '==', phone)
    .get()
    .then(snapshot =>
      snapshot.docs.map(doc => ({
        ...doc.data(),
        id: doc.id,
      })),
    );

export const updateUserApi = (id, updatedField) => {
  return db
    .collection('users')
    .doc(id)
    .set({...updatedField}, {merge: true});
};
