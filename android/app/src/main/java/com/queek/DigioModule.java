package com.queek;

import androidx.appcompat.app.AppCompatActivity;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;

import org.jetbrains.annotations.Nullable;

import in.digio.sdk.kyc.DigioEnvironment;
import in.digio.sdk.kyc.DigioKycConfig;
import in.digio.sdk.kyc.DigioKycResponseListener;
import in.digio.sdk.kyc.DigioSession;

public class DigioModule extends ReactContextBaseJavaModule implements DigioKycResponseListener {
    DigioModule(ReactApplicationContext context){
        super(context);
    }

    @Override
    public String getName() {
        return "DigioModule";
    }

    @ReactMethod
    public void initiateDigioKycProcess() {
        try {
            DigioKycConfig config = new DigioKycConfig();
            config.setEnvironment(DigioEnvironment.PRODUCTION);

            DigioSession digioSession = new DigioSession();
            digioSession.init((AppCompatActivity)getCurrentActivity(), config);
            digioSession.startSession("KID2202251228566578PSXRO4ZMSTWY6", "9205707912","GWT2202251228566848BUAGIJU9DIQPG",this);// this refers //DigioKycResponseListener
        } catch(Exception e) {

        }
    }

    @Override
    public void onDigioKycSuccess(@Nullable String s, @Nullable String s1) {

    }

    @Override
    public void onDigioKycFailure(@Nullable String s, @Nullable String s1) {

    }
}